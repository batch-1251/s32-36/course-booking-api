//we will use this for authentication
	//login
	//retreive use details

//Json Web Token
	//methods: 
		//sign(data, secret, {})       -creates a token
		//verify(token, secret, cb())  -checks if the token is present
		//decode(token, {}).payload     -interpret/decodes the created token

let jwt=require('jsonwebtoken');
let secret='CourseBookingAPI';

//create a token
module.exports.createAccessToken=(user)=>{
	const data={id:user._id, email: user.email, isAdmin:user.Admin}
	return jwt.sign(data,secret,{});
}

//verify token
module.exports.verify=(req, res, next)=>{
	let token=req.headers.authorization
	// console.log(token)
	if(typeof token!=="undefined"){
		token=token.slice(7, token.length);
		return jwt.verify(token,secret,(error,data)=>{
			if(error){return res.send({auth:"failed"})}
				else{next()}
		})

	}
}
//decode token
module.exports.decode=(token)=>{
		if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
			// return jwt.decode(token,{complete:true}).payload
			return jwt.verify(token,secret,(error,data)=>{if(error)
				{
					return null
				}
				else {return jwt.decode(token,{complete:true}).payload}
			})
	
	}

}
// decode(req.headers.authorization)