const express=require('express');
const router=express.Router();


const userController=require("./../controllers/userControllers");
const auth=require("./../auth");
//check if email exist
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExist(req.body).then(result=>res.send(result))
})

//user registration
router.post("/register",(req,res)=>{
	userController.register(req.body).then(result=>res.send(result));
})
//login
router.post('/login',(req,res)=>{
	userController.login(req.body).then(result=>res.send(result))
})
	//details
	router.get("/details", auth.verify,(req,res)=>{
		const userData=auth.decode(req.headers.authorization)
		// console.log(userData)
		userController.getProfile(userData.id).then(result=>res.send(result))
	})

//erollments
router.post('/enroll',auth.verify,(req, res)=>{
	let data={
		userId:auth.decode(req.headers.authorization).id,
		courseId:req.body.courseId

	}
	// console.log(data)
	userController.enroll(data).then(result=>res.send(result))

})

module.exports=router;