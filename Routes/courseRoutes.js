const express = require('express');
const router = express.Router();
let auth=require('./../auth')

//controllers
const courseController = require('./../controllers/courseControllers');

//retrieve active courses
router.get('/active', (req, res) => {

	courseController.getAllActive().then( result => res.send(result));
}) 

//retrieve all courses
router.get('/all', (req, res) => {

	courseController.getAllCourses().then( result => res.send(result));
})

//add course
router.post('/addCourse', auth.verify,(req, res) => {

	courseController.addCourse(req.body).then( result => res.send(result));
}) 
//make a route to get single course
router.get('/:courseId', auth.verify, (req, res)=>{
	courseController.getSingleCourse(req.params).then(result=>res.send(result));
})
//make route to update the course
router.put('/:courseId/edit', auth.verify, (req,res)=>{
	// consol.log(result);
	courseController.editCourse(req.params.courseId, req.body).then(result=>res.send(result));
})
//make a route to archive the course
router.put('/:courseId/archive', auth.verify, (req,res)=>{
	courseController.archiveCourse(req.params.courseId).then(result=>res.send(result));
})
//make a route to unarchive the course
router.put('/:courseId/unarchive', auth.verify, (req,res)=>{
	courseController.unarchiveCourse(req.params.courseId).then(result=>res.send(result));
})

//make a route to delete the course
router.delete('/:courseId/deleteCourse', auth.verify, (req,res)=>{
	courseController.deleteCourse(req.params.courseId).then(result=>res.send(result));
})

module.exports=router;