const User=require("./../models/User");
const Course=require('./../models/Courses');
const bcrypt=require('bcrypt');
const auth=require('./../auth'); 

module.exports.checkEmailExist=(reqbody)=>{
	return User.find({email:reqbody.email}).then((result)=>{
		if(result.length!=0){return true}
			else{return false}
	})
}
module.exports.register=(reqbody)=>{
	let newUser=new User({
		firstName:reqbody.firstName,
		lastName:reqbody.lastName,
		email:reqbody.email,
		password:bcrypt.hashSync(reqbody.password,10),
		mobileNo:reqbody.mobileNo
	})

	return newUser.save().then((result,error)=>{
		if(error){return error}
			else{return true}
	})
}
module.exports.login=(reqbody)=>{
	//model.metjod
	return User.findOne({email:reqbody.email}).then((result)=>{
		if(result==null){return false}
			else{
				const isPasswordCorrect=bcrypt.compareSync(reqbody.password, result.password)
				if(isPasswordCorrect===true){
				 return {access:auth.createAccessToken(result.toObject())}
				}else{return false}
			}
	})
}
module.exports.getProfile=(data)=>{
  return User.findById(data).then(result=>{
  	result.password="******"
  	return result
  })
}

// module.exports.enroll=async(data)=>{
// //save user enrollments
// const userSaveStatus=await User.findById(data.userId).then(user=>{
// 	user.enrollments.push({courseId:data.courseId})

// 	return user.save().then((user, error)=>{
// 		if(error){return false}else{return true}
// 	}
// )}



// //save course enrolees
// const courseSaveStatus=await Course.findById(data.courseId).then(course=>{
// 	course.enrollees.push({userId: data.userId})

// 	return course.save().then((course, error)=>{
// 		if(error){return false}else{return true}
// 	}
// })
// if(userSaveStatus && courseSaveStatus){return true}else{false}


// }
module.exports.enroll = async (data) => {

	//save user enrollments
	const userSaveStatus = await User.findById(data.userId).then( user => {
		// console.log(user)
		user.enrollments.push({courseId: data.courseId})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	});

	//save course enrollees
	const courseSaveStatus = await Course.findById(data.courseId).then( (course) => {
		course.enrollees.push({userId: data.userId});

		return course.save().then( (course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})
	if(userSaveStatus && courseSaveStatus){
			return true
		} else {
			return false
		}

}