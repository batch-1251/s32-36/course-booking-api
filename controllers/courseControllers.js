const Course = require('./../models/Courses')

module.exports.getAllActive = () => {
	//Model.method
	return Course.find({isActive: true}).then( result => {
		
		return result
	})
} 

module.exports.addCourse=(reqbody)=>{
	// console.log(reqbody)
	let newCourse=new Course({
		name: reqbody.name,
		description: reqbody.description,
		price: reqbody.price
	});


	return newCourse.save().then((course,error)=>{
		if(error){return false}
			else{return true}
})
}

module.exports.getAllCourses=()=>{
	return Course.find().then(result=>result)
}
module.exports.getSingleCourse=(params)=>{
	return Course.findById(params.courseId).then(result=>{
		return result
	})
}


//edit course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//archive course
module.exports.archiveCourse=(params)=>{
	let updateActiveCourse={
		isActive:false
	}
	return Course.findByIdAndUpdate(params, updateActiveCourse, {new:true}).then((result,error)=>{
		if(error){return false}else{return true}
	})
}
//unarchive course
module.exports.unarchiveCourse=(params)=>{
	let updateActiveCourse={
		isActive:true
	}
	return Course.findByIdAndUpdate(params, updateActiveCourse, {new:true}).then((result,error)=>{
		if(error){return false}else{return true}
	})
}
//delete course
module.exports.deleteCourse=(params)=>{
	return Course.findByIdAndDelete(params).then((result,error)=>{
		if(error){return false}else{return true}
	})
}