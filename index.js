const express=require('express');
const mongoose=require('mongoose');
const cors=require('cors');


const PORT=process.env.PORT || 3000; 
const app=express();


//from routes
let userRoutes=require("./routes/userRoutes");
let courseRoutes=require("./routes/courseRoutes");

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}))
app.use(cors());




mongoose.connect("mongodb+srv://louie011:lowiidr011@cluster0.7ajhf.mongodb.net/course_booking?retryWrites=true&w=majority",
		{useNewUrlParser: true, useUnifiedTopology: true}
	).then(()=>console.log(`Connected to Database`)).catch((error)=>console.log(error));

//Schema

//routes
app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes)


app.listen(PORT, ()=>{console.log(`Server running on port ${PORT}`)})